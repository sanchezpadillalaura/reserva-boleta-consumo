var token = sessionStorage.getItem('token');
    if (isEmpty(token)) {
      window.location = 'index.html';
    }
  	$(document).ready(function() {
      $('#emailLogin').text(sessionStorage.getItem('emailLogin'))
  		$('#verDisponibilidad').on('click', function(){
  			obtenerListado(1);
  		})

      $('#verMisReservas').on('click', function(){
        obtenerListadoReservado(1);
      })

      $('#btn-logout').on('click', function(){
        logout();
      })
  	})

    var url_base = 'http://127.0.0.1:8000/api/v1';

    function obtenerListado(page) {

      $.ajax({
        url: url_base+"/tickets?page="+page,
        type: "GET",
        dataType: "json",
        headers: {
          'Authorization':'Bearer '+token
        },
      })
      .done(function (res) {
        var data = res.data.data
        if (res.data.total>0) {
          procesarListado(data, false)
        }else{
          swal("¡Disculpe!", 'No hya boletas disponibles', "info");
        }
      })
      .fail(function (res) { 
        swal("¡Disculpe!", res.responseJSON.message, "error");
      });
    }

    function obtenerListadoReservado(page) {
      $.ajax({
        url: url_base+"/tickets/booking/?page="+page,
        type: "GET",
        dataType: "json", 
        headers: {
          'Authorization':'Bearer '+token
        },
      })
      .done(function (res) {
        var data = res.data.data
        if (res.data.total>0) {
          procesarListado(data, true)
        }else{
          swal("¡Disculpe!", 'No encontramos ninguna reserva', "info");
        }
      })
      .fail(function (res) { 
        swal("¡Disculpe!", res.responseJSON.message, "error");
      });
    }

    function reservar(id) {
      $('#btn-reservar'+id).text('Reservando...').attr('disabled', 'disabled')
      $.ajax({
        url: url_base+"/tickets/"+id+"/booking",
        type: "POST",
        dataType: "json",
        headers: {
          'Authorization':'Bearer '+token
        },
      })
      .done(function (res) {
        $('#li'+id).remove()
        swal("Bien!", res.message, "success");
      })
      .fail(function (res) { 
        swal("¡Disculpe!", res.responseJSON.message, "error");
        $('#btn-reservar'+id).removeAttr('disabled')
      });
    }

    function logout() {
      $('#btn-logout').text('Saliendo...').attr('disabled', 'disabled')
      $.ajax({
        url: url_base+"/logout",
        type: "POST",
        dataType: "json",
        headers: {
          'Authorization':'Bearer '+token
        },
      })
      .done(function (res) {
        sessionStorage.removeItem('token');
        window.location = 'index.html';
      })
      .fail(function (res) { 
        swal("¡Disculpe!", res.responseJSON.message, "error");
        $('#btn-logout').removeAttr('disabled')
      });
      
    }

    function procesarListado(data, me) {
      $('#listadoBoleta').html('')
      let reservar ='';
      data.forEach(function( index , item) {
        if (!me) {
          reservar = '<a onclick="reservar('+index.id+')" id="btn-reservar'+index.id+'" class="btn btn-warning ">Reservar </a>'
        }

        $('#listadoBoleta').append('<li id="li'+index.id+'" class="list-group-item">'+(item+1)+'- '+index.title+' '+reservar+'</li>')
      });
      $('.card-disponibilidad').show()

      // if (res.last_page>1) {
      //   $('#pagination').show()
      //   $('#pagination .current_page').html(res.current_page )
      // }
    }

    function isEmpty(obj){
      var isEmpty = false;
   
      if (typeof obj == 'undefined' || obj === null || obj === ''){
        isEmpty = true;
      }      
         
      if (typeof obj == 'number' && isNaN(obj)){
        isEmpty = true;
      }
         
      if (obj instanceof Date && isNaN(Number(obj))){
        isEmpty = true;
      }
         
      return isEmpty;
    }