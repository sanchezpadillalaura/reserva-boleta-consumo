var url_base = 'http://127.0.0.1:8000/api/v1';

  	$(document).ready(function() {
  		$('#formLogin').on('submit', function(e){
  			e.preventDefault();
  			$.ajax({
                url: url_base+ "/login",
                type: "POST",
                dataType: "json",
                data: $( this ).serialize()
            })
            .done(function (res) {
                sessionStorage.setItem('token',res.access_token);
                sessionStorage.setItem('emailLogin',$('#emailLogin').val());
            	window.location = 'reserva.html';

            })
            .fail(function (res) { 
                swal("¡Disculpe!", res.responseJSON.message, "error");
            });
  		})

  		$('#formRegister').on('submit', function(e){
  			e.preventDefault();
  			let data = $( this ).serialize();
  			$.ajax({
                url: url_base +"/register",
                type: "POST",
                dataType: "json",
                data: data
            })
            .done(function (res) {
            	$.ajax({
	                url: url_base +"/login",
	                type: "POST",
	                dataType: "json",
	                data: data
	            })
	            .done(function (res) {
	                sessionStorage.setItem('token',res.access_token);
	            	window.location = 'reserva.html';

	            })
	            .fail(function (res) { 
	                swal("¡Disculpe!", res.responseJSON.message, "error");
	            });

            })
            .fail(function (res) { 
                swal("¡Disculpe!", res.responseJSON.message, "error");
            });
  		})
  	})